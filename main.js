// 1) Це структура (древо) що складається з html

// 2) innerText - тільки текст;
//    innerHTML - текст та елементи;

// 3)  document.getElementById
//     але самий універсальний метод це document.querySelectorAll



let paragraph = document.querySelectorAll('p');
for (elem of paragraph) {
    elem.style.background= '#ff0000';
}  

let optionsList = document.getElementById('optionsList');
console.log(optionsList);

let padreOptionsList = optionsList.parentElement.nodeName;
console.log(padreOptionsList);

for ( let item of optionsList.childNodes) {
    console.log(item.nodeName + ' ' + item.nodeType);
};

let testParagraph = document.getElementById('testParagraph');
testParagraph.innerText = "This is a paragraph";

let mainHeader = document.querySelectorAll('.main-header');
for (let item of mainHeader) {

item.classList.add("nav-item");
} 

let sectionTitle = document.querySelectorAll('.section-title');
for (let item of sectionTitle) {
    item.classList.remove('section-title');
}